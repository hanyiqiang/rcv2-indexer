package ir.rcv2.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.util.Tool;

import ir.rcv2.json.Posting;
import ir.rcv2.mapreduce.reducers.PostingReducerHBase;
import ir.rcv2.nlp.NlpUtils;
import opennlp.tools.tokenize.WhitespaceTokenizer;

public class UniWordQuery extends Configured implements Tool {
	@SuppressWarnings("resource")
	@Override
	public int run(String[] args) throws Exception {
		// Connect to HBase
		try (Connection conn = ConnectionFactory.createConnection(this.getConf())) {
			// Connect to table
			Table table = conn.getTable(TableName.valueOf(this.getConf().get("TABLE_NAME")));
    		System.out.printf("======== Connection to table %s successful ========\n", this.getConf().get("TABLE_NAME"));
			
			// Ask for user query
			Scanner keyboard = new Scanner(System.in);
			System.out.println("Enter your search queries below (Ctrl+d to exit):");
			
			while(true) {			
				String query = keyboard.nextLine();					// Raw user query
				List<String> queryTerms = queryTerms(query);		// List of terms
				Result[] results = queryTable(table, queryTerms);	// HBase result set
				
				// Parse result set and calculate query vector using IDF weights
				Map<Integer,Document> documents = new LinkedHashMap<Integer,Document>();
				Map<String, Double> queryVector = new LinkedHashMap<String, Double>();
				for (Result result : results) {
					// Get posting JSON from HBase result
					byte[] val = result.getValue(PostingReducerHBase.POSTINGS_BYTES, 
												 PostingReducerHBase.POSTINGS_BYTES);
					
					// Generate documents from postings
					if (val != null) {
						Posting posting = Posting.fromJSON(Bytes.toString(val));
						queryVector.put(posting.getTerm(), posting.getIdf());
						for (int docID : posting.getDocIDs()) {
							if (documents.containsKey(docID)) {
								Document doc = documents.get(docID);
								doc.addTerm(posting.getTerm(), 
											posting.getTfScores().get(docID), 
											posting.getIdf());
							} else {
								Document doc = new Document();
								doc.setDocID(docID);
								doc.addTerm(posting.getTerm(), 
											posting.getTfScores().get(docID), 
											posting.getIdf());
								documents.put(docID, doc);
							}
						}
					}
				}
				
				// Rank documents and print results
				List<Document> rankedDocuments = new ArrayList<Document>(documents.values());
				rank(queryVector, rankedDocuments);
				
				// Print top results
				int numResults = this.getConf().getInt("NUM_RESULTS", 10);
				System.out.printf("Showing top %d results out of %d\n", numResults, rankedDocuments.size());
				for (int i = 0; i < numResults && i < rankedDocuments.size(); i++) {
					System.out.println(rankedDocuments.get(i));
				}
				
				// Wait for next query
				System.out.println("= = = = = = = = =");
				if(keyboard.hasNextLine()) {
					continue;
				} else {
					break;
				}
			}			
			
			// Close any stream/connections
			conn.close();
		} catch (IOException e) {
			e.printStackTrace();
			return 1;
		}
		
		return 0;
	}
	
	private void rank(Map<String, Double> queryVector, List<Document> documents) {
		// Calc vector length, then normalize
		final double qLength = Math.sqrt(queryVector.values().stream().mapToDouble(o -> o*o).sum());
		queryVector.forEach((term,score) -> queryVector.put(term,score/qLength));
		
		System.out.print("Normalized query vector... [");
		queryVector.values().forEach((score) -> System.out.printf("%.4f ", score));
		System.out.println("]");
		
		// Calc document vectors and rank via cosine similarity
		for (Document doc : documents) {
			// Calc document vector and normalize
			final double dLength = Math.sqrt(doc.getTermTfIdfScores().values().stream().mapToDouble(o -> o*o).sum());
			Map<String,Double> docVector = doc.getTermTfIdfScores();
			docVector.forEach((term, score) -> docVector.put(term, score/dLength));
			
			// Cosine similarity 
			double rank = 0.0;
			for (String queryTerm : queryVector.keySet()) {
				double qScore = queryVector.get(queryTerm);
				// Remember, the document might not contain query term
				double dScore = docVector.containsKey(queryTerm) ? docVector.get(queryTerm) : 0.0;
				rank += qScore * dScore;
			}
			
			// Set computed rank
			doc.setRank(rank);
		}
		
		// Sort on rank
		Collections.sort(documents);
	}
	
	// Clean query and return search terms
	private List<String> queryTerms(String query) {
		List<String> terms = new ArrayList<String>(); 
		terms.addAll(Arrays.asList(WhitespaceTokenizer.INSTANCE.tokenize(query)));
		NlpUtils.cleanTokens(terms);	
		return terms;
	}
	
	// Get results from HBase
	private Result[] queryTable(Table table, List<String> terms) throws IOException {
		// Create list of Get operations
		List<Get> getTerms = new ArrayList<Get>();
		for (String term : terms) {
			Get get = new Get(Bytes.toBytes(term));
			get.addColumn(PostingReducerHBase.POSTINGS_BYTES, PostingReducerHBase.POSTINGS_BYTES);
			getTerms.add(get);
		}	
		return table.get(getTerms);
	}
}
