package ir.rcv2.query;

import java.util.LinkedHashMap;
import java.util.Map;

public class Document implements Comparable<Document>{
	private int docID;
	private Map<String,Double> termTfScores;
	private Map<String,Double> termIdfScores;
	private Map<String,Double> termTfIdfScores;
	private double rank = 0;
	
	public Document() {
		termTfScores = new LinkedHashMap<String,Double>();
		termIdfScores = new LinkedHashMap<String,Double>();
		termTfIdfScores = new LinkedHashMap<String,Double>();
	}
	
	public int getDocID() {
		return docID;
	}

	public void setDocID(int docID) {
		this.docID = docID;
	}

	public Map<String, Double> getTermTfScores() {
		return termTfScores;
	}

	public Map<String, Double> getTermIdfScores() {
		return termIdfScores;
	}
	
	public Map<String, Double> getTermTfIdfScores() {
		return termTfIdfScores;
	}
	
	public void addTerm(String term, double tf, double idf) {
		this.termTfScores.put(term, tf);
		this.termIdfScores.put(term, idf);
		this.termTfIdfScores.put(term, tf*idf);
	}

	public void setRank(double rank) {
		this.rank = rank;
	}
	
	public double getRank() {
		return this.rank;
	}
	
	@Override
	public int compareTo(Document o) {
		double result = this.rank - o.getRank();
		if (result > 0) {
			return -1;
		} else if (result < 0){
			return 1;
		} else {
			return this.docID - o.getDocID();
		}
	}

	public String toString() {
		StringBuilder docString = new StringBuilder();
		
		docString.append("Rank=").append(String.format("%.4f",this.rank)).append(" ");
		docString.append("<");
			docString.append(this.docID).append(": ");
			for (String term : this.termTfIdfScores.keySet()) {
				docString.append("[")
						 .append(term)
						 .append(": tf-idf=")
						 .append(String.format("%.4f", this.termTfIdfScores.get(term)))
						 .append("]");
			}
		docString.append(">");
		
		return docString.toString();
	}
}
