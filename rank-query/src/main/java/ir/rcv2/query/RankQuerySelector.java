package ir.rcv2.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class RankQuerySelector extends Configured implements Tool
{
	private static final Map<Class<? extends Tool>, String> KNOWN_QUERY_TOOLS;
	
	// Define all tools we can run
	static {
		Map<Class<? extends Tool>, String> tools = new LinkedHashMap<Class<? extends Tool>, String>();
		
		// Known tools + descriptions
		tools.put(UniWordQuery.class, "Query an HBase table with UniWord postings");
		
		KNOWN_QUERY_TOOLS = Collections.unmodifiableMap(tools);
	};
	
    @SuppressWarnings("resource")
	public static void main( String[] args ) throws Exception
    {	
    	Configuration conf = new Configuration();
    	Tool tool = null;
    	
    	// Connect to HBase/Zookeeper
    	try(Connection conn = ConnectionFactory.createConnection(conf)) {
    		System.out.println("======== Connection to HBase successful ========");
    		
    		// Ask user to select HBase table
    		Admin admin = conn.getAdmin();
    		TableName[] tables = admin.listTableNames();
    		Scanner keyboard = new Scanner(System.in);
    		while(true) {  			
    			System.out.printf("Please select an HBase table to query (1..%d):\n", tables.length);
        		int num = 1;
        		for (TableName table : tables) {
        			System.out.printf("%2d. %20s\n", num, table.getNameAsString());
        			num++;
        		}
        		
        		// Get user choice and set Table on conf if valid
        		int choice = keyboard.nextInt();
        		if (choice > 0 && choice <= tables.length) {
        			conf.set("TABLE_NAME", tables[choice-1].getNameAsString());
        			break;
        		}
    		}
 
    		// Ask user to select Query Tool
    		while(true) {  			
    			System.out.printf("Please select a query tool (1..%d):\n", KNOWN_QUERY_TOOLS.size());
        		int num = 1;
        		for (Class<? extends Tool> queryClass : KNOWN_QUERY_TOOLS.keySet()) {
        			System.out.printf("%2d. %20s : %s\n", 
        					num, queryClass.getSimpleName(), KNOWN_QUERY_TOOLS.get(queryClass));
        			num++;
        		}
        		
        		// Get user choice and set up tool if valid
        		int choice = keyboard.nextInt();
        		if (choice > 0 && choice <= KNOWN_QUERY_TOOLS.size()) {
            		ArrayList<Class<? extends Tool>> tools = 
            				new ArrayList<Class<? extends Tool>>(KNOWN_QUERY_TOOLS.keySet());
            		tool = tools.get(choice-1).newInstance();
            		if (tool != null) {
            			break;
            		}
        		}
    		}    		
    		
    		// Cleanup
    		admin.close();
    		conn.close();
    	} catch (IOException e) {
    		e.printStackTrace();
    		System.exit(1);
    	}

    	// Run query tool
    	System.exit(ToolRunner.run(conf, tool, args));
    }

	@Override
	public int run(String[] args) throws Exception {
		return 0;
	}
}
