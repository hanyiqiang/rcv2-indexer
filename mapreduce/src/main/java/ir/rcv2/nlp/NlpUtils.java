package ir.rcv2.nlp;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class NlpUtils {
	private static final String[] STOPWORDS = {
			"a", "an", "and", "are", "as", "at", 
			"be", "by", "for", "from", "has", "he", "her",
			"him", "his", "i", "in", "is", "it", 
			"its", "of", "on", "that", "the", "to", 
			"was", "were", "will", "with", "you", "your"
	};
	
	private static final String NON_WORD_REGEX = ".*[^a-zA-Z0-9]+.*";
	private static final String YEAR_REGEX = "^[1-2][0-9]{3}$";
	private static final String NUMERIC_REGEX = ".*\\d+.*";
	
	private static final List<String> STOPWORD_LIST = Arrays.asList(NlpUtils.STOPWORDS);
	
	/** ============= REGEX TESTING============== **/	
	// Test if given string is a year in the range 1000-2999
	public static boolean isYear(String s) {
		return s.matches(YEAR_REGEX);
	}
	
	// Does string contain ANY numeric characters?
	public static boolean isNumeric(String s) {
		return s.matches(NUMERIC_REGEX);
	}
	
	// Does string start with non alpha-numeric character?
	public static boolean isNonWord(String s) {
		return s.matches(NON_WORD_REGEX);
	}
	/** ==================================== **/
	
	/** =============== LAMBDA REPLACEMENTS =============== **/
	// Remove any any token that begins with a non-alphanumeric character
	private static void replaceNonWordSequences(List<String> tokens) {
		tokens.replaceAll((String s) -> isNonWord(s) ? "" : s);
	}
	
	private static void replaceNumeric(List<String> tokens) {
		tokens.replaceAll((String s) -> isNumeric(s) ? "" : s);
	}
	
	// Remove all stop symbols from tokens
	private static void replaceApostrophe(List<String> tokens) {
		tokens.replaceAll((String s) -> s.replaceAll("[']", ""));
	}
	
	// Remove any null tokens from list
	private static void removeSingleCharacters(List<String> tokens) {
		Iterator<String> iter = tokens.iterator();

		while (iter.hasNext()) {
		    String token = iter.next();
			if(token.length() <= 1) {
				iter.remove();
			}
		}
	}
	
	private static void removeStopwords(List<String> tokens) {
		tokens.removeAll(NlpUtils.STOPWORD_LIST);
	}
	
	private static void toLower(List<String> tokens) { 
		tokens.replaceAll((String s) -> s.toLowerCase()); 
	}
	/** ==================================== **/
	
	/** ============= PUBLIC CLEAN METHOD =============== **/
	public static void cleanTokens(List<String> tokens) {
		// Clean all input of extraneous synbols
		replaceApostrophe(tokens);
		replaceNonWordSequences(tokens);
		replaceNumeric(tokens);

		// Remove from list after completing all replacement ops
		removeSingleCharacters(tokens);
		
		// Normalize at the end, since the list should be smaller now
		toLower(tokens);
		removeStopwords(tokens);
	}
	
	public static void generateBiWords(List<String> tokens) {
		for(int i = 0; i < tokens.size() -1; i++) {
			tokens.set(i, tokens.get(i) + tokens.get(i+1));
		}
	}
	/** ==================================== **/
}
