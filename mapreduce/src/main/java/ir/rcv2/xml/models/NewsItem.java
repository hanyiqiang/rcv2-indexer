package ir.rcv2.xml.models;

public class NewsItem {
	private String text;
	private int id;
	private String title;
	
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getID() {
		return this.id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String toString() {
		return this.title + ":" + this.id;
	}
	
}
