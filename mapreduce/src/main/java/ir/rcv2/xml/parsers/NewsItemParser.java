package ir.rcv2.xml.parsers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import ir.rcv2.xml.handlers.NewsItemHandler;
import ir.rcv2.xml.models.NewsItem;

public class NewsItemParser {
	private SAXParserFactory factory;
	private SAXParser parser;
	private ByteArrayInputStream xmlStream;
	private NewsItemHandler handler;
	
	public NewsItemParser() {
		try {
			factory = SAXParserFactory.newInstance();
			parser = factory.newSAXParser();
			handler = new NewsItemHandler();
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
	}
	
	public NewsItem parseArticle(byte[] xml) {
		try {
			xmlStream = new ByteArrayInputStream(xml);
			parser.parse(xmlStream, handler);
			xmlStream.reset();
			xmlStream.close();
			return handler.getArticle();
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
