package ir.rcv2.json;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PositionalPosting {
	private static ObjectMapper mapper;
	
	static {
		mapper = new ObjectMapper();
	}
	
	public static PositionalPosting fromJSON(String json) {
		try {
			return mapper.readValue(json, PositionalPosting.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String toJSON(PositionalPosting posting) {
		try {
			return mapper.writeValueAsString(posting);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	int frequency;
	String key;
	Map<Integer, List<Integer>> postings;
	
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public Map<Integer, List<Integer>> getPostings() {
		return postings;
	}
	public void setPostings(Map<Integer, List<Integer>> postings) {
		this.postings = postings;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getKey() {
		return this.key;
	}
	public String toString(){
		return this.key + ":" + this.frequency + " " + postings.toString();
	}
}
