package ir.rcv2.json;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Posting {
	private static ObjectMapper mapper;
	
	static {
		mapper = new ObjectMapper();
	}
	
	public static Posting fromJSON(String json) {
		try {
			return mapper.readValue(json, Posting.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String toJSON(Posting posting) {
		try {
			return mapper.writeValueAsString(posting);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	String term;
	Integer docFreq;
	Double idf;
	List<Integer> docIDs;
	Map<Integer,Double> tfScores;
	
	public Posting() {
		docIDs = new LinkedList<Integer>();
		tfScores = new HashMap<Integer,Double>();
	}
	
	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public Integer getDocFreq() {
		return docFreq;
	}

	public void setDocFreq(Integer frequency) {
		this.docFreq = frequency;
	}

	public Double getIdf() {
		return idf;
	}

	public void setIdf(Double idf) {
		this.idf = idf;
	}

	public List<Integer> getDocIDs() {
		return docIDs;
	}

	public void setDocIDs(List<Integer> docIDs) {
		this.docIDs = docIDs;
	}

	public Map<Integer, Double> getTfScores() {
		return tfScores;
	}

	public void setTfScores(Map<Integer, Double> tfScores) {
		this.tfScores = tfScores;
	}
	
}
