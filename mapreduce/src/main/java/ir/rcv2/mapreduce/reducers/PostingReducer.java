package ir.rcv2.mapreduce.reducers;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ir.rcv2.json.Posting;

public class PostingReducer extends Reducer<Text, Text, Text, Text> {
	private Text postingsJSON;
	
	@Override
	public void setup(Context context) {
		postingsJSON = new Text();
	}
	
	@Override
	public void reduce(Text key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {
		// Our aggregate posting and a placeholder to for the encoded JSON objects
		Posting current;
		Posting aggregate = new Posting();
		
		// Aggregator variables
		List<Integer> allDocIDs = new LinkedList<Integer>();
		Map<Integer, Double> allTfScores = new HashMap<Integer, Double>();
		
		// Deserialize each JSON posting and aggregate
		for(Text val : values) {
			current = Posting.fromJSON(val.toString());
			List<Integer> currentDocIDs = current.getDocIDs();
			Map<Integer, Double> currentTfScores = current.getTfScores();
			for(Integer docID : currentDocIDs) {
				allDocIDs.add(docID);
				allTfScores.put(docID, currentTfScores.get(docID));
			}
		}
		
		// Set final values
		aggregate.setTerm(key.toString());
		aggregate.setDocFreq(allDocIDs.size());
		double D = context.getConfiguration().getInt("D",0);							// Total size of corpus
		double idf = Math.log10(D/(1.0 + (double)aggregate.getDocFreq()));	// Calc idf score
		aggregate.setIdf(idf);
		aggregate.setDocIDs(allDocIDs);
		aggregate.setTfScores(allTfScores);
		
		postingsJSON.set(Posting.toJSON(aggregate));
		
		context.write(key,postingsJSON);		
	}
}
