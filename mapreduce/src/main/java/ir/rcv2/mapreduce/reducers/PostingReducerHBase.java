package ir.rcv2.mapreduce.reducers;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;

import ir.rcv2.json.Posting;

public class PostingReducerHBase extends TableReducer<Text, Text, ImmutableBytesWritable> {
	public static final Map<String,String> TABLE_COLUMNS;
	public static final String TERM = "term";
	public static final byte[] TERM_BYTES = Bytes.toBytes(TERM);
	public static final String POSTINGS = "postings";
	public static final byte[] POSTINGS_BYTES = Bytes.toBytes(POSTINGS);
	
	static {
		Map<String,String> map = new HashMap<String,String>();
		map.put(TERM,TERM);
		map.put(POSTINGS,POSTINGS);
		TABLE_COLUMNS = Collections.unmodifiableMap(map);
	}
	
	ImmutableBytesWritable keyOut;
	
	@Override
	public void setup(Context context) {
		keyOut = new ImmutableBytesWritable();
	}
	
	@Override
	public void reduce(Text key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {
		// Our aggregate posting and a placeholder to for the encoded JSON objects
		Posting current;
		Posting aggregate = new Posting();
		
		// Aggregator variables
		List<Integer> allDocIDs = new LinkedList<Integer>();
		Map<Integer, Double> allTfScores = new HashMap<Integer, Double>();
		
		// Deserialize each JSON posting and aggregate
		for(Text val : values) {
			current = Posting.fromJSON(val.toString());
			List<Integer> currentDocIDs = current.getDocIDs();
			Map<Integer, Double> currentTfScores = current.getTfScores();
			for(Integer docID : currentDocIDs) {
				allDocIDs.add(docID);
				allTfScores.put(docID, currentTfScores.get(docID));
			}
		}
		
		// Set final values
		aggregate.setTerm(key.toString());
		aggregate.setDocFreq(allDocIDs.size());
		double D = context.getConfiguration().getInt("D",0);				// Total size of corpus
		double idf = Math.log10(D/(1.0 + (double)aggregate.getDocFreq()));	// Calc idf score
		aggregate.setIdf(idf);
		aggregate.setDocIDs(allDocIDs);
		aggregate.setTfScores(allTfScores);
		
		// Create Put operation
		Put put = new Put(Bytes.toBytes(key.toString()));
		
		// Add column information
		put.addColumn(TERM_BYTES, TERM_BYTES, Bytes.toBytes(aggregate.getTerm()));
		put.addColumn(POSTINGS_BYTES, POSTINGS_BYTES, Bytes.toBytes(Posting.toJSON(aggregate)));
		
		// This is ignored anyway
		keyOut.set(Bytes.toBytes(aggregate.getTerm()));
		
		// Write to HBase
		context.write(keyOut, put);		
	}
}
