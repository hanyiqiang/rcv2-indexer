package ir.rcv2.mapreduce.reducers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ir.rcv2.json.PositionalPosting;

public class PositionalPostingReducer extends Reducer<Text, Text, Text, Text> {
	private Text postingsJSON;
	
	@Override
	public void setup(Context context) {
		postingsJSON = new Text();
	}
	
	@Override
	public void reduce(Text key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {
		PositionalPosting current;
		PositionalPosting aggregate = new PositionalPosting();
		int frequency = 0;
		Map<Integer, List<Integer>> allPostings = new HashMap<Integer, List<Integer>>();
		
		for(Text val : values) {
			current = PositionalPosting.fromJSON(val.toString());
			frequency += current.getFrequency();
			Map<Integer, List<Integer>> currentPostings = current.getPostings();
			for(Integer docID : currentPostings.keySet()) {
				allPostings.put(docID, currentPostings.get(docID));
			}
		}
		
		aggregate.setFrequency(frequency);
		aggregate.setPostings(allPostings);
		aggregate.setKey(key.toString());
		
		postingsJSON.set(PositionalPosting.toJSON(aggregate));
		
		context.write(key,postingsJSON);		
	}
}
