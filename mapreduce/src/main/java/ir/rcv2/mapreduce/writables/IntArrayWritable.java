package ir.rcv2.mapreduce.writables;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;

public class IntArrayWritable extends ArrayWritable {

    public IntArrayWritable() {
        super(IntWritable.class);
    }
    
    @Override
    public String toString() {
        IntWritable[] values = (IntWritable[]) super.get();
        StringBuilder outputBuilder = new StringBuilder();
        
        for(IntWritable i : values) {
        	outputBuilder.append(i).append(" ");
        }
        
        return outputBuilder.toString();
    }
}
