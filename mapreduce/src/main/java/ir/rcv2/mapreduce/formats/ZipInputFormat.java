package ir.rcv2.mapreduce.formats;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.*;

/**
 * This input format simply instructs Hadoop to not split the Zip archives
 * and send each one to a Mapper, storing the entire archive in memory. 
 * */
public class ZipInputFormat extends FileInputFormat<Text, BytesWritable> {
	/** 
	 * Always send whole files to mapper. 
	 * */
    @Override
    protected boolean isSplitable(JobContext context, Path filename) {
    	return false;
    }

	@Override
	public RecordReader<Text, BytesWritable> createRecordReader(
			InputSplit split, TaskAttemptContext taskContext) throws IOException,
			InterruptedException {
		return new ZipRecordReader();
	}
}
