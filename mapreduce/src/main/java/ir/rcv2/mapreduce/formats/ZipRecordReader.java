package ir.rcv2.mapreduce.formats;

import java.io.IOException;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

/**
 * All this really does is read a zip archive into memory and prepare it
 * for further processing in the Mapper. 
 * */
public class ZipRecordReader extends RecordReader<Text, BytesWritable> {
    private FileSplit split;
    private Path path;
	private FSDataInputStream fsin;
	private boolean isFinished = false;
    
	// Records
	private Text currentKey = null;
	private BytesWritable currentValue = null;
	
	@Override
	public void initialize(InputSplit inputSplit, TaskAttemptContext taskContext)
			throws IOException, InterruptedException {
        this.split = (FileSplit) inputSplit;
        this.path = split.getPath();
        FileSystem fs = path.getFileSystem( taskContext.getConfiguration() );
        this.fsin = fs.open( path );
        this.currentKey = new Text(path.toString());
	}    
    
	@Override
	public void close() throws IOException {
		try { 
			this.fsin.close(); 
		} catch (Exception ignore) {}
	}

	@Override
	public float getProgress() throws IOException {
		return this.isFinished ? 1f : 0f;
	}

	@Override
	public Text getCurrentKey() throws IOException, InterruptedException {
		return currentKey;
	}

	@Override
	public BytesWritable getCurrentValue() throws IOException, InterruptedException {
		return currentValue;
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		if(fsin.available() == 0) {
			this.isFinished = true;
			return false;
		}
		
		if(currentValue == null) {
			currentValue = new BytesWritable();
		}
		
		// Set Value
		byte[] buffer = new byte[(int)split.getLength()];
		fsin.readFully(buffer);	
		currentValue.set(buffer, 0, buffer.length);
		
		return true;
	}

}
