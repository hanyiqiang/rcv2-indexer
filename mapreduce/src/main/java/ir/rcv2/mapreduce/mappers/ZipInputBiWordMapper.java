package ir.rcv2.mapreduce.mappers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.zip.ZipInputStream;

import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import ir.rcv2.mapreduce.writables.IntArrayWritable;
import ir.rcv2.nlp.NlpUtils;
import ir.rcv2.xml.models.NewsItem;
import ir.rcv2.xml.parsers.NewsItemParser;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.WhitespaceTokenizer;

public class ZipInputBiWordMapper extends Mapper<Text, BytesWritable, Text, IntArrayWritable> {
	private static final int BUFF_SIZE = 8192;
	
	// Read ZIP entries
	private ByteArrayInputStream bytes;
	private ZipInputStream zip;
	
	// Parse ZIP entries
	private NewsItemParser parser;
	private NewsItem article;
	private Tokenizer tokenizer;
	private String[] tokens;
	private LinkedList<String> token_list;
	private HashSet<String> token_set;
	
	// Output records
	private Text word;
	private IntWritable docID;
	private IntArrayWritable postings;
	
	@Override
	protected void setup(Context context) 
			throws IOException, InterruptedException {
		word = new Text();
		docID = new IntWritable();
		postings = new IntArrayWritable();
		parser = new NewsItemParser();
		tokenizer = WhitespaceTokenizer.INSTANCE;
		token_list = new LinkedList<String>();
		token_set = new HashSet<String>();
	}
	
	@Override
	protected void cleanup(Context context) 
			throws IOException {
		zip.close();
		bytes.close();
	}
	
	@Override
	public void map(Text key, BytesWritable value, Context context) 
			throws IOException, InterruptedException {
		bytes = new ByteArrayInputStream(value.getBytes());
		zip = new ZipInputStream(bytes);
		
		ByteArrayOutputStream outBytes = new ByteArrayOutputStream();		
        byte[] buff = new byte[BUFF_SIZE];
		
		// Read each ZipEntry XML file
		// Getting the next entry positions the ZIP stream for reading. 
		while(zip.getNextEntry() != null) {			
			// If we have a valid entry, we need to read its contents in to memory
			while ( true ) {
				int bytesRead = 0;
				
				try { 
					bytesRead = zip.read( buff, 0, BUFF_SIZE ); 
				} catch ( EOFException e ) { 
					break;
				}

				if ( bytesRead > 0 ) { 
					outBytes.write( buff, 0, bytesRead ); 
				} else { 
					break; 
				}
	        }
			
        	// Decompress ZIP entry
			article = parser.parseArticle(outBytes.toByteArray());
			docID.set(article.getID());
			
			// Use OpenNLP trained tokenizer
			String content = article.getTitle() + " " + article.getText();
			tokens = tokenizer.tokenize(content);
			
			// Clean tokens
			token_list.addAll(Arrays.asList(tokens));
			NlpUtils.cleanTokens(token_list);
			
			// Convert to BiWords
			NlpUtils.generateBiWords(token_list);
			
			// By adding them to a set, we can do a per document combine
			token_set.addAll(token_list);		
			for(String token : token_set) {
				word.set(token);
				IntWritable[] postingList = new IntWritable[1];
				postingList[0] = docID;
				postings.set(postingList);
				context.write(word, postings);
			}
			
			// Cleanup and get ready for next record
			token_list.clear();
			token_set.clear();
			outBytes.reset();
			zip.closeEntry();
		}
		outBytes.close();
	}
}
