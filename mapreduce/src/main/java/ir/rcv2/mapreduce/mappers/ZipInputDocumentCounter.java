package ir.rcv2.mapreduce.mappers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.zip.ZipInputStream;

import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class ZipInputDocumentCounter extends Mapper<Text, BytesWritable, NullWritable, NullWritable> {
	
	// Read ZIP entries
	private ByteArrayInputStream bytes;
	private ZipInputStream zip;

	public enum Count {
		TOTAL_DOCUMENTS;
	}
	
	@Override
	protected void setup(Context context) 
			throws IOException, InterruptedException {
	}
	
	@Override
	protected void cleanup(Context context) 
			throws IOException {
		zip.close();
		bytes.close();
	}
	
	@Override
	public void map(Text key, BytesWritable value, Context context) 
			throws IOException, InterruptedException {
		bytes = new ByteArrayInputStream(value.getBytes());
		zip = new ZipInputStream(bytes);
		
		// Read each ZipEntry XML file
		// Getting the next entry positions the ZIP stream for reading. 
		while(zip.getNextEntry() != null) {			
			context.getCounter(Count.TOTAL_DOCUMENTS).increment(1);
			zip.closeEntry();
		}
	}
}
