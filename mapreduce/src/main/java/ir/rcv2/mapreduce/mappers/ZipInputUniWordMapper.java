package ir.rcv2.mapreduce.mappers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import ir.rcv2.json.Posting;
import ir.rcv2.nlp.NlpUtils;
import ir.rcv2.xml.models.NewsItem;
import ir.rcv2.xml.parsers.NewsItemParser;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.WhitespaceTokenizer;

public class ZipInputUniWordMapper extends Mapper<Text, BytesWritable, Text, Text> {
	private static final int BUFF_SIZE = 8192;
	
	// Read ZIP entries
	private ByteArrayInputStream bytes;
	private ZipInputStream zip;
	
	// Parse ZIP entries
	private NewsItemParser parser;
	private NewsItem article;
	private Tokenizer tokenizer;
	private String[] tokens;
	private LinkedList<String> token_list;
	
	// Document Indexing
	private Map<String, Integer> termFreqIndex;
	private Posting posting;
	
	// Output records
	private Text word;
	private Text postingJSON;
	
	@Override
	protected void setup(Context context) 
			throws IOException, InterruptedException {
		word = new Text();
		postingJSON = new Text();
		parser = new NewsItemParser();
		tokenizer = WhitespaceTokenizer.INSTANCE;
		token_list = new LinkedList<String>();
		posting = new Posting();
	}
	
	@Override
	protected void cleanup(Context context) 
			throws IOException {
		zip.close();
		bytes.close();
	}
	
	@Override
	public void map(Text key, BytesWritable value, Context context) 
			throws IOException, InterruptedException {
		bytes = new ByteArrayInputStream(value.getBytes());
		zip = new ZipInputStream(bytes);
		
		ByteArrayOutputStream outBytes = new ByteArrayOutputStream();		
        byte[] buff = new byte[BUFF_SIZE];
		
		// Read each ZipEntry XML file
		// Getting the next entry positions the ZIP stream for reading. 
		while(zip.getNextEntry() != null) {			
			// If we have a valid entry, we need to read its contents in to memory
			while ( true ) {
				int bytesRead = 0;
				
				try { 
					bytesRead = zip.read( buff, 0, BUFF_SIZE ); 
				} catch ( EOFException e ) { 
					break;
				}

				if ( bytesRead > 0 ) { 
					outBytes.write( buff, 0, bytesRead ); 
				} else { 
					break; 
				}
	        }
			
        	// Decompress ZIP entry
			article = parser.parseArticle(outBytes.toByteArray());
			
			// Use OpenNLP trained tokenizer
			String content = article.getTitle() + " " + article.getText();
			tokens = tokenizer.tokenize(content);
			
			// Clean tokens
			token_list.addAll(Arrays.asList(tokens));
			NlpUtils.cleanTokens(token_list);
			
			// Keep track of term frequency in this document
			termFreqIndex = new HashMap<String, Integer>();
			
			// Build term frequency map for document
			for(String token : token_list) {
				if(termFreqIndex.containsKey(token)) {
					termFreqIndex.put(token, termFreqIndex.get(token) + 1);
				} else {
					termFreqIndex.put(token, 1);
				}
			}
			
			// Generate JSON to write to context
			for(String s : termFreqIndex.keySet()) {
				// Calculate document metrics
				int docID = article.getID();
				int frequency = termFreqIndex.get(s);
				double tf = 1 + Math.log10(frequency);
				double idf = 0;	// We leave idf as 0, it must be calculated in the reducer
				
				// List of docIDs, will always be of size 1
				List<Integer> docIDs = new LinkedList<Integer>();
				docIDs.add(docID);
				
				// Map of docID to tf score, will always have one entry
				Map<Integer, Double> tfScores = new HashMap<Integer, Double>();
				tfScores.put(docID, tf);
				
				// Update positional posting
				posting.setTerm(s);
				posting.setDocFreq(docIDs.size());
				posting.setIdf(idf);
				posting.setDocIDs(docIDs);
				posting.setTfScores(tfScores);
				
				// Write to context
				word.set(s);
				postingJSON.set(Posting.toJSON(posting));
				context.write(word, postingJSON);
			}
			
			// Cleanup and get ready for next record
			token_list.clear();
			outBytes.reset();
			zip.closeEntry();
		}
		outBytes.close();
	}
}
