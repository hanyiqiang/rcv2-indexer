package ir.rcv2.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class ToolSelector extends Configured implements Tool{
	private static final Map<Class<? extends Tool>, String> KNOWN_TOOLS;
	
	// Define all tools we can run
	static {
		Map<Class<? extends Tool>, String> tools = new LinkedHashMap<Class<? extends Tool>, String>();
		
		// Known tools + descriptions
		tools.put(UniWord.class, "UniWord Index which outputs to an index file");
		tools.put(UniWordHBase.class, "UniWord Index which outputs to HBase table");
		
		KNOWN_TOOLS = Collections.unmodifiableMap(tools);
	};
	
    public static void main(String[] args) throws Exception {
    	if(args.length != 2) {
    		System.err.println("USAGE: yarn jar index-1.0.0.jar <input> <output>");
    		System.exit(1);
    	}

    	Scanner keyboard = new Scanner(System.in);
    	Tool tool;
    	
    	while(true) {
        	System.out.printf("Please select an index tool (1 .. %d)\n", KNOWN_TOOLS.size());
        	int num = 1;
        	for (Class<? extends Tool> tool_class : KNOWN_TOOLS.keySet()) {
        		System.out.printf("%2d. %20s : %s\n", num, tool_class.getSimpleName(), KNOWN_TOOLS.get(tool_class));
        		num++;
        	}
        	// Get user choice
        	int choice = keyboard.nextInt();
        	
        	if (choice > 0 && choice <= KNOWN_TOOLS.size()) {
        		ArrayList<Class<? extends Tool>> tools = new ArrayList<Class<? extends Tool>>(KNOWN_TOOLS.keySet());
        		tool = tools.get(choice-1).newInstance();
        		if (tool != null) {
        			break;
        		}
        	}
    	}
    	
    	keyboard.close();
    	System.exit(ToolRunner.run(new Configuration(), tool, args));
    }
	
	@Override
	public int run(String[] args) throws Exception {
		return 0;
	}

}