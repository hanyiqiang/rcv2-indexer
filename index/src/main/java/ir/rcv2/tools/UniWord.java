package ir.rcv2.tools;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import ir.rcv2.mapreduce.formats.ZipInputFormat;
import ir.rcv2.mapreduce.mappers.ZipInputDocumentCounter;
import ir.rcv2.mapreduce.mappers.ZipInputUniWordMapper;
import ir.rcv2.mapreduce.reducers.PostingReducer;

public class UniWord extends Configured implements Tool{
	@Override
	public int run(String[] args) throws Exception {
	    Configuration conf = this.getConf();
	    conf.setInt("D", (int) CountDocuments(args));
	    
	    Job job = Job.getInstance(conf, this.getClass().getSimpleName());
	    job.setJarByClass(this.getClass());
	    
	    // Use custom input format
	    job.setInputFormatClass(ZipInputFormat.class);
	    
	    // Configure Mapper
	    job.setMapperClass(ZipInputUniWordMapper.class);
	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(Text.class);
	    
	    // Configure Combiner
	    job.setCombinerClass(PostingReducer.class);  
	    
	    // Configure Reducer
	    job.setReducerClass(PostingReducer.class);
	    job.setGroupingComparatorClass(Text.Comparator.class); 
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	    job.waitForCompletion(true);			    
	    
		return 0;
	}
	
	// Count number of documents, needed for IDF calc
	private long CountDocuments(String[] args) 
			throws IOException, ClassNotFoundException, InterruptedException {
	    Configuration conf = new Configuration();
	    
	    Job job = Job.getInstance(conf, "Document Counter");
	    job.setJarByClass(this.getClass());
	    
	    // Use custom input format
	    job.setInputFormatClass(ZipInputFormat.class);
	    
	    // Configure Mapper
	    job.setMapperClass(ZipInputDocumentCounter.class);
	    job.setMapOutputKeyClass(NullWritable.class);
	    job.setMapOutputValueClass(NullWritable.class);
	    
	    // No Reduce phase
	    job.setNumReduceTasks(0);
	    
	    // Execute job
	    Path inPath = new Path(args[0]);
	    FileInputFormat.addInputPath(job, inPath);
	    Path outPath = new Path(args[1]);
	    FileOutputFormat.setOutputPath(job, outPath);
	    job.waitForCompletion(true);
	    
	    // Delete output directory for use in other jobs
	    FileSystem fs = FileSystem.get(new Configuration());
	    fs.delete(outPath, true);
	    
	    return job.getCounters().findCounter(ZipInputDocumentCounter.Count.TOTAL_DOCUMENTS).getValue();
	}

}