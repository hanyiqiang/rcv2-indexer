package ir.rcv2.tools;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import ir.rcv2.mapreduce.formats.ZipInputFormat;
import ir.rcv2.mapreduce.mappers.ZipInputDocumentCounter;
import ir.rcv2.mapreduce.mappers.ZipInputUniWordMapper;
import ir.rcv2.mapreduce.reducers.PostingReducerHBase;

public class UniWordHBase extends Configured implements Tool{
	private static final String TABLE_NAME = UniWordHBase.class.getSimpleName();
	
	@Override
	public final int run(String[] args) throws Exception {
		// Get Configuration and add HBase information
	    Configuration conf = this.getConf();
	   
	    // Create HBase table
	    Table table = null;
	    try(Connection conn = ConnectionFactory.createConnection(conf)) {
			// Create table schema
			TableName name = TableName.valueOf(TABLE_NAME);
		    HTableDescriptor descriptor = new HTableDescriptor(name);
		    
		    for(String col : PostingReducerHBase.TABLE_COLUMNS.keySet()) {
		    	descriptor.addFamily(new HColumnDescriptor(col));
		    }
		    
		    // Use admin to commit table schema
		    Admin admin = conn.getAdmin();
		    
		    // Check if table already exists, drop if it does
		    try {
		    	admin.createTable(descriptor);
		    } catch (TableExistsException e) {
		    	admin.disableTable(descriptor.getTableName());
		    	admin.deleteTable(descriptor.getTableName());
			    admin.createTable(descriptor);
		    }
		    
		    // Get Table to return for OutputFormat
		    table = conn.getTable(descriptor.getTableName());
		    
		    // Close connection
		    admin.close();
		    conn.close();
		} catch (IOException e) {
			e.printStackTrace();
			return 1;
		}
	    
	    if (table != null) {
		    // Set document count
	    	int documentCount = (int) countDocuments(args);
	    	
	    	if (documentCount > 0) {
	    		conf.setInt("D", documentCount);
	    	} else {
	    		return 1;
	    	}

	    	// Get HBase MR job ready
		    Job job = Job.getInstance(conf, UniWordHBase.class.getSimpleName());
		    job.setJarByClass(ZipInputFormat.class);
		    TableMapReduceUtil.initTableReducerJob(TABLE_NAME, PostingReducerHBase.class, job);
		    
		    // Use custom input format
		    job.setInputFormatClass(ZipInputFormat.class);
		    
		    // Configure Mapper
		    job.setMapperClass(ZipInputUniWordMapper.class);
		    job.setMapOutputKeyClass(Text.class);
		    job.setMapOutputValueClass(Text.class);
		    
		    // Configure Reducer		    
		    job.setGroupingComparatorClass(Text.Comparator.class); 
		    job.setReducerClass(PostingReducerHBase.class);
		    
		    // Input from HDFS, output to HBase table
		    FileInputFormat.addInputPath(job, new Path(args[0]));
		    
			return job.waitForCompletion(true) ? 0 : 1;	
	    } else {
	    	return 1;
	    }
	}
	
	// Count number of documents, needed for IDF calc
	private long countDocuments(String[] args) 
			throws IOException, ClassNotFoundException, InterruptedException {    
	    Job job = Job.getInstance(new Configuration(), "Document Counter");
	    job.setJarByClass(UniWordHBase.class);
	    
	    // Use custom input format
	    job.setInputFormatClass(ZipInputFormat.class);
	    
	    // Configure Mapper
	    job.setMapperClass(ZipInputDocumentCounter.class);
	    job.setMapOutputKeyClass(NullWritable.class);
	    job.setMapOutputValueClass(NullWritable.class);
	    
	    // No Reduce phase
	    job.setNumReduceTasks(0);
	    
	    // Execute job
	    Path inPath = new Path(args[0]);
	    FileInputFormat.addInputPath(job, inPath);
	    Path outPath = new Path(args[1]);
	    FileOutputFormat.setOutputPath(job, outPath);
	    
	    if(job.waitForCompletion(true)) {
		    // Delete output directory for use in other jobs
		    FileSystem fs = FileSystem.get(new Configuration());
		    fs.delete(outPath, true);
		    
		    return job.getCounters().findCounter(ZipInputDocumentCounter.Count.TOTAL_DOCUMENTS).getValue();	
	    } else {
	    	return 0;
	    }
	}
	
}