# RCV2-IR-System #

### What is this repository for? ###

* Run MapReduce jobs to created inverted indices for the Zipped RCV2 data
* v1.0.0

### How do I get set up? ###

* Summary: The easiest way to use this project is to import it as a Maven Project in Eclipse Neon
* Configuration: The preferred Maven goal set is "clean compile package"
* Dependencies: Maven handles all dependencies
* Deployment: Running the Maven build will produce two main JAR files for use, see below:

### indexer-1.0.0.jar ###

* Location: jars/indexer-1.0.0.jar
* Contains MapReduce jobs to create UniWord, BiWord, and UniWordPositional index files
* USAGE: hadoop jar indexer-1.0.0 <Uniword|BiWord|Positional> <input_dir> <output_dir>

### query-1.0.0.jar ###

* Location: jars/query-1.0.0.jar
* Contains interactive boolean query search of a given index
* USAGE: java -jar query-1.0.0 <UniWord|BiWord|Positional> <input_index_file>
